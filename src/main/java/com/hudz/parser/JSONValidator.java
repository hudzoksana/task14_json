package com.hudz.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import java.io.File;
import java.io.IOException;

public class JSONValidator {

    public static boolean validateJSON(String jsonPath, String schemaPath)
            throws IOException, ProcessingException {
        final JsonNode data = JsonLoader.fromString(jsonPath);
        final JsonNode schema = JsonLoader.fromString(schemaPath);

        final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonValidator validator = factory.getValidator();
        ProcessingReport report = validator.validate(schema, data);
        return report.isSuccess();
    }
}
