package com.hudz.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hudz.model.Deposit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JSONParser {

    private ObjectMapper objectMapper;

    public JSONParser() {
        this.objectMapper = new ObjectMapper();
    }
    public List<Deposit> getDepositList(String json) {
        List<Deposit> depositList = new ArrayList<>();
        try {
            depositList = Arrays.asList(objectMapper.readValue(json, Deposit[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return depositList;
    }
}
