package com.hudz.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.hudz.model.Deposit;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GSONParser {

    private Gson gson;

    public GSONParser() {
        this.gson = new Gson();
    }
    public List<Deposit> getDepositList(String json) {
        return Arrays.asList(gson.fromJson(json, Deposit[].class));
    }
}
