package com.hudz;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.hudz.comparator.DepositComparator;
import com.hudz.model.Deposit;
import com.hudz.parser.GSONParser;
import com.hudz.parser.JSONParser;
import com.hudz.parser.JSONValidator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Parser {
  private final static Logger logger = LogManager.getLogger(Parser.class);

  public static void main(String[] args) {


    JSONParser JSparser = new JSONParser();
    GSONParser gsonParser = new GSONParser();
    try {
      String json = new String(Files.readAllBytes(Paths
              .get("src//main//resources//json//Banks.json")));
      String schemaPath = new String(Files.readAllBytes(Paths.get("src//main//resources//json//BanksSchema.json")));

      if (JSONValidator.validateJSON(json, schemaPath)) {
        System.out.println("JSON");
        printList(JSparser.getDepositList(json));
        System.out.println("GSON");
        printList(gsonParser.getDepositList(json));
      } else {
        System.out.println("Not valid!");
      }
    } catch (IOException|ProcessingException e) {
      e.printStackTrace();
    }
  }
  private static void printList(List<Deposit> planes) {
    Collections.sort(planes, new DepositComparator());
    for (Deposit plane : planes) {
      logger.trace(plane);
    }
  }
}
