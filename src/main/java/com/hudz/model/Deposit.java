package com.hudz.model;

import java.util.List;

public class Deposit {

    private String name;
    private String country;
    private List<String> types;
    private String depositor;
    private int accountID;
    private int amountOfDeposit;
    private double profitability;
    private double timeConstrains;

    public Deposit() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> type) {
        this.types = type;
    }

    public String getDepositor() {
        return depositor;
    }

    public void setDepositor(String depositor) {
        this.depositor = depositor;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public int getAmountOfDeposit() {
        return amountOfDeposit;
    }

    public void setAmountOfDeposit(int amountOfDeposit) {
        this.amountOfDeposit = amountOfDeposit;
    }

    public double getProfitability() {
        return profitability;
    }

    public void setProfitability(double profitability) {
        this.profitability = profitability;
    }

    public double getTimeConstrains() {
        return timeConstrains;
    }

    public void setTimeConstrains(double timeConstrains) {
        this.timeConstrains = timeConstrains;
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", type=" + types +
                ", depositor='" + depositor + '\'' +
                ", accountID=" + accountID +
                ", amountOfDeposit=" + amountOfDeposit +
                ", profitability=" + profitability +
                ", timeConstrains=" + timeConstrains +
                '}';
    }
}
